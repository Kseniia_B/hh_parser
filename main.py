import multiprocessing  # для многопотчности
from helpers import write_to_excel, get_search_lists, drop_duplicates, unpack
from parsers import parse_resumes, parse_vacancies

if __name__ == '__main__':

    dev_terms_a = ['разработчик', 'программист', 'developer']
    dev_terms_b = ['Java', 'JavaScript', 'JS', 'TypeScript', 'TS', '.Net', 'C#', 'Python']
    dev_search_list = get_search_lists(dev_terms_a, dev_terms_b)
    sys_terms_a = ['системный аналитик', 'system analyst']
    sys_terms_b = ['BPMN', 'UML', 'XML', 'DFD', 'IDEF**', 'ER']
    sys_search_list = get_search_lists(sys_terms_a, sys_terms_b)

    search_lists = [
        ['аналитик', 'analyst'],
        ['analyst', 'аналитик'],
        ['разработчик', 'developer'],
        ['developer', 'разработчик'],
        ['техписатель', 'технический писатель'],
        ['технический писатель', 'техписатель'],
        ['ручной тестировщик', 'тестировщик', 'qa'],
        ['автоматизатор', 'тестировщик', 'developer'],
        ['devops'],
        ['начальник', 'руководитель', 'менеджер', 'lead'],
    ]

    with multiprocessing.Pool(len(search_lists)) as pool:
        vacancies = pool.map(parse_vacancies, search_lists)
    vacancies = unpack(vacancies)
    vacancies = drop_duplicates(vacancies)
    write_to_excel(vacancies, 'parsed_data/vacancies')

    with multiprocessing.Pool(len(search_lists)) as pool:
        resumes = pool.map(parse_resumes, search_lists)
    resumes = unpack(resumes)
    resumes = drop_duplicates(resumes)
    write_to_excel(resumes, 'parsed_data/resumes')
