import re

import requests  # выполняет HTTP-запросы
from bs4 import BeautifulSoup as bs  # работа с HTML

from base_parser import BaseParser
from helpers import standard_handler, unpack


class ResumesParser(BaseParser):
    def __init__(self):
        super().__init__()
        #  Конфигурация поиска
        self._MAIN_URL_ARGS = {
            'text': '',
            'st': 'resumeSearch',
            'logic': 'normal',  # все слова в названии
            'pos': 'position',
            'exp_period': 'all_time',
            'exp_company_size': 'any',
            'exp_industry': 'any',
            'specialization': '1',
            'area': '1261',  # 1261 - код Свердловской области
            'relocation': 'living_or_relocation',
            'salary_from': '',
            'salary_to': '',
            'currency_code': '',
            'education': 'none',
            'age_from': '',
            'age_to': '',
            'gender': 'unknown',
            'order_by': 'relevance',
            'search_period': '30',  # за последние 30 дней
            'items_on_page': '20',
        }

        #  Хедеры для requests
        self._HEADERS = {
            "User-Agent": "Chrome/74.0.3729.169",
        }

        #  Основа url поиска
        self._MAIN_URL_PATTERN = "https://hh.ru/search/resume"

        #  Конфигурация поиска искомых элементов (вакансия, резюме)
        self._CARD_SEARCH_CONFIG = {
            'tag_name': 'div',
            'tag_attribute': 'class',
            'attribute_value': 'resume-search-item',
            'contains': False,
        }

        #  Конфигурация поиска кнопки пагинатора
        self._PAGINATOR_BUTTON_SEARCH_CONFIG = {
            'tag_name': 'a',
            'tag_attribute': 'class',
            'attribute_value': 'bloko-button',
            'contains': True,
        }

        #  Конфигурация поиска полей, которые нас интересуют
        self._FIELDS = [
            {'title': 'Название', 'tag_name': 'a', 'tag_attribute': 'class',
             'attribute_value': 'resume-search-item__name', 'contains': False, 'handler': standard_handler},
            {'title': 'Зарплата', 'tag_name': 'div', 'tag_attribute': 'class',
             'attribute_value': 'resume-search-item__compensation', 'contains': False, 'handler': self.salary_handler},
            {'title': 'Опыт работы', 'tag_name': 'div', 'tag_attribute': 'class',
             'attribute_value': 'resume-search-item__description-content', 'contains': False,
             'handler': standard_handler},
            {'title': 'Возраст', 'tag_name': 'span', 'tag_attribute': 'data-qa',
             'attribute_value': 'resume-serp__resume-age', 'contains': False, 'handler': standard_handler},
            {'title': 'url резюме', 'tag_name': 'a', 'tag_attribute': 'data-qa',
             'attribute_value': 'resume-serp__resume-title', 'contains': False, 'handler': self.url_handler},
        ]

    def set_main_url(self, search_strings):
        """
        Устанавливает основной url поиска.

        :param search_strings: Список искомых совпадений.
        :type search_strings: List[str]
        :rtype: None
        """
        search_string = search_strings[0].strip().replace(' ', '+').replace('#', '%23')
        self._MAIN_URL_ARGS['text'] = search_string

        #  Основной url поиска
        self._MAIN_URL = self.get_main_url(self._MAIN_URL_PATTERN, self._MAIN_URL_ARGS)

    @staticmethod
    def get_main_url(url, args):
        """
        Возвращает адрес поиска с указанными аргументами.

        :param url: Основа url поиска.
        :type url: str
        :param args: Аргументы поиска.
        :type args: Dict
        :return: Адрес поиска.
        :rtype: str
        """
        if not args:
            return url
        words = [f'{k}={v}' for k, v in args.items()]
        return url + '?' + '&'.join(words)

    @staticmethod
    def get_main_url_text(request):
        """
        Возвращает значение аргумента 'text' для поиска нужного запроса в "hh.ru".
        Убирает лишние пробелы по бокам, заменяет пробелы между словами на **+**.
    ​
        :param request: Строка запроса.
        :type request: str
        :return: Значение аргумента *text*.
        :rtype: str
        """
        words = request.strip().split()
        q = '+'.join(words).lower()
        return q

    def salary_handler(self, salary):
        """
        Обработчик информации о заработной плате.

        :param salary: Строка, содержащяя информацию о зарплате.
        :return: Возвращает информацию о диапазоне зарплаты и валюту.
        :rtype: str
        """
        salary = salary.text
        if not salary:
            return "Не указано"
        salary = re.sub(r'\s+', '', salary)
        salary_value = int(re.findall(r'\d+', salary)[0])
        salary_currency = re.findall(r'[^\d]+', salary)[0]
        salary_currency = self._converter.abbr_dict[salary_currency]
        # words = salary.strip().split()
        # salary_value = int(words[0])  # зп
        # currency = words[1]  # Валюта зп
        if not 'RUR' in salary_currency:
            salary_value = self._converter.convert_to_rur(salary_value, salary_currency)
        currency = 'RUR'
        return f'{salary_value} {currency}'

    @staticmethod
    def url_handler(link):
        """
        Обработчик ссылки на резюме.

        :param link: Элемент с ссылкой.
        :return: Ссылку на резюме.
        :rtype: str
        """
        if not link:
            return "Не указано"
        url = 'https://ekaterinburg.hh.ru' + link.get('href')
        url = url.split('?')[0]
        return url

    def get_html(self, url):
        """
        Возвращает html-код страницы.
    ​
        :param url: Адрес страницы.
        :type url: str
        :return: html-код страницы.
        :rtype: bs4.BeautifulSoup
        """
        r = requests.get(url, headers=self._HEADERS)
        html = bs(r.content, 'html.parser')
        return html

    @staticmethod
    def get_page_url(main_url, number):
        """
        Возвращает адрес страницы с указанным номером.
    ​
        :param main_url: Главный адрес поиска.
        :type main_url: str
        :param number: Номер страницы.
        :type number: int
        :return: Адрес страницы поиска с указанным номером.
        :rtype: str
        """
        return f'{main_url}&page={number}'

    @staticmethod
    def get_select(html, params):
        """
        Возвращает результат поиска в переданном html-контенте
        элемента(ов) с указанными параметрами поиска.
        Параметр *params* должен быть словарем с полями:
            * "tag_name": "имя тэга"
            * "tag_attribute": "имя аттрибута"
            * "attribute_value": "значение атрибута"
            * "contains": искать ли по частичному значению атрибута
        По этим параметрам будут найдены искомые элементы.

        :param html: Контент, в которой производится поиск.
        :type html: Union[bs4.element.Tag, bs4.BeautifulSoup]
        :param params: Словарь с параметрами поиска элементов.
        :type params: Dict[str, str]
        :return: Список с найденными элементами.
        :rtype: List[str]
        """
        return html.select(
            f'{params["tag_name"]}[{params["tag_attribute"]}'
            f'{"*" if params["contains"] else ""}="{params["attribute_value"]}"]')

    def get_pages_count(self, main_url, params):
        """
        Возвращает количество страниц поиска.
        Нужно передать параметры поиска элементов *params* для кнопки пагинатора.
        Параметр params должен быть словарем с полями:
            * "tag_name": "имя тэга"
            * "tag_attribute": "имя аттрибута"
            * "attribute_value": "значение атрибута"
            * "contains": искать ли по частичному значению атрибута
        По этим параметрам будут найден номер последней кнопки пагинатора.
    ​
        :param main_url: Главный адрес поиска.
        :type main_url: str
        :param params: Словарь с параметрами поиска элементов.
        :type params: Dict[str, str]
        :return: Количество страниц поиска.
        :rtype: int
        """
        html = self.get_html(main_url)
        paginator = self.get_select(html, params)
        if paginator:
            try:
                pages_count = int(paginator[-2].text)
                return pages_count
            except:
                return 1
        else:
            return 1

    def parser(self, page_number):
        """
        Парсит страницу по указаному номеру.
    ​
        :param page_number: Номер страницы, с которой собирается информация.
        :type page_number: int
        :return: Список словарей, содержащий информацию из искомых элементов.
        :rtype: List[Dict[str, str]]
        """
        result_list = []
        html = self.get_html(self.get_page_url(self._MAIN_URL, page_number))
        cards = self.get_select(html, self._CARD_SEARCH_CONFIG)
        for card in cards:
            card_info = {}
            for params in self._FIELDS:
                field = self.get_select(card, params)
                if field:
                    card_info[params["title"]] = params["handler"](field[0])
                else:
                    card_info[params["title"]] = params["handler"]("")
            result_list.append(card_info)
        return result_list

    def get_parsed_data(self, search_strings, only_matching=True):
        """
        Возвращает список искомых данных.

        :param search_strings: Строки поиска.
        :type search_strings: List[str]
        :param only_matching: Пропускать ли несовпадаюшие.
        :type only_matching: bool
        :return: Лист словарей с найдеными данными.
        :rtype: List[Dict[str, Union[str, int]]]
        """
        self.set_main_url(search_strings)  # устанавиливаем строку поиска

        # определение количества страниц
        pages_amount = self.get_pages_count(self._MAIN_URL, self._PAGINATOR_BUTTON_SEARCH_CONFIG)

        pages_data = []
        for page in range(pages_amount):
            pages_data.append(self.parser(page))  # парсинг страниц

        result_list = unpack(pages_data)  # распаковка результата

        if only_matching:
            # удаление данных с несовпадающими названиями
            result_list = self.get_without_unmatched(result_list, search_strings)
        return result_list
