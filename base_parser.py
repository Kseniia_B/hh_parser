from helpers import CurrencyConverter
from helpers import CustomTranslator


class BaseParser:
    def __init__(self):
        #  Конвертер валют
        self._converter = CurrencyConverter()
        #  Переводчик
        self.translator = CustomTranslator()

    @staticmethod
    def get_without_unmatched(data, matches):
        """
        Возвращает данные без элементов,
        в названиях которых не присутствует хотя бы одно из переданных слов.

        :param data: Данные для обработки.
        :type data: List[Dict[str, Union[str, int]]]
        :param matches: Совпадения.
        :type matches: List[str]
        :return: Обработанные данные.
        :rtype: List[Dict[str, Union[str, int]]]
        """
        result = []
        for item in data:
            name = item['Название'].lower()
            for match in matches:
                is_matched = all([word in name for word in match.lower().split()]) \
                             or all([word in name for word in match.lower().split('-')])
                if is_matched:
                    result.append(item)
                    break
        return result
