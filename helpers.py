import re

import pandas as pd
import requests
from translate import Translator


def unpack(list_of_lists):
    """
    Распаковывает список списков.
​
    :param list_of_lists: Список списков для распаковки.
    :type list_of_lists: List[List[Any]]
    :return: Распакованный список из элементов.
    :rtype: List[Any]
    """
    flat_list = []
    for sublist in list_of_lists:
        for item in sublist:
            flat_list.append(item)
    return flat_list


def get_search_lists(terms_a, terms_b):
    """
    Возвращает списки для поиска и нужные синонимы для проверки совпадения.

    :param terms_a: Главный термин.
    :type terms_a: List[str]
    :param terms_b: Дополнительные слова, специализации, и т.д.
    :type terms_b: List[str]
    :return: Лист из списков нужных терминов для поиска для проверки совпадения.
    :rtype: List[List[str]]
    """
    result = []
    for term_a in terms_a:
        for term_b in terms_b:
            result.append([term_a + ' ' + term_b])
    return result


def drop_duplicates(data):
    """
    Удаляет строки дубликаты из данных.
    Возвращает данные в виде pandas.core.frame.DataFrame.

    :param data: List[Dict]
    :return: Датафрейм с уникальными строками.
    :rtype: pandas.core.frame.DataFrame
    """
    df = pd.DataFrame(data)
    return df.drop_duplicates()


def write_to_excel(elements_list, file_name):
    """
    Записывает информацию в Excel фаил.

    :param elements_list: Список словарей с информацией.
    :type elements_list: List[Dict[str, str]]
    :param file_name: Название файла, в который нужно записать данные.
    :type file_name: str
    :rtype: None
    """
    excel_file_name = file_name + '.xlsx'
    f = open(excel_file_name, 'w')
    f.close()

    dt = pd.DataFrame(elements_list)
    dt.to_excel(excel_file_name)


def get_request_json(url, args):
    """
    Возвращает json get-запроса по переданным адресу и аргументам.

    :param url: Основа url запроса. Не передавать через него аргументы, передавать через *args*
    :type url: str
    :param args: Аргументы запроса.
    :type args: Dict[str, Any]
    """
    return requests.get(url, params=args).json()


def standard_handler(value):
    """
    Стандартный обработчик поля.
    Возвращает 'Не указано', если поле пустое.
​
    :param value: Значение поля.
    :return: Обработаная строка поля.
    :rtype: str
    """
    if not value:
        return "Не указано"
    value = value.text if type(value) != str else value
    return value.replace(chr(160), ' ').replace('\u2009', ' ')


class CurrencyConverter:
    """
    Конвертер валют. Достает курс вылют с сайта hh.ru.
    Перед использованием нужно проинициализировать.

    """
    def __init__(self):
        self.dict_url = 'https://api.hh.ru/dictionaries'
        self.currencies_dict = self._fetch_currencies_dict()
        self.abbr_dict = {currency_info['abbr']: abbr for abbr, currency_info in self.currencies_dict.items()}
        self.abbr_dict['бел.руб.'] = self.abbr_dict.pop('бел. руб.')

    def _fetch_currencies_dict(self):
        response = get_request_json(self.dict_url, {})['currency']
        result = {
            currency['code']: {
                'abbr': standard_handler(currency['abbr']),
                'name': standard_handler(currency['name']),
                'rate': currency['rate']
            } for currency in response
        }
        return result

    def convert_to_rur(self, amount, currency_code):
        """
        Конвертирует валюту в рубли.

        :param amount: Сумма.
        :type amount: int
        :param currency_code: Код валюты.
        :type currency_code: str
        :return: Конвертированная сумма.
        :rtype: str
        """
        for currency_abbr, currency_info in self.currencies_dict.items():
            if currency_code in currency_info['abbr']:
                currency_code = currency_abbr
                break
        return round(amount / self.currencies_dict[currency_code]['rate'])


class CustomTranslator:
    def __init__(self):
        #  Переводчик с русского на английский
        self.ru_to_en_translator = Translator(to_lang='en', from_lang='ru')

        #  Переводчик с английского на русский
        self.en_to_ru_translator = Translator(to_lang='ru', from_lang='en')

        #  Регулярная функция для отброса лишних символов
        self.regex_en = re.compile('[^a-zA-Z ]')

        #  Регулярная функция для отброса лишних символов
        self.regex_ru = re.compile('[^а-яА-Я ]')

    def translate_ru_to_en(self, string):
        result = self.ru_to_en_translator.translate(string).lower()
        result = self.regex_en.sub('', result)
        return result

    def translate_en_to_ru(self, string):
        result = self.en_to_ru_translator.translate(string).lower()
        result = self.regex_ru.sub('', result)
        if 'the' in result:
            result = result.replace('the', '').strip()
        return result
