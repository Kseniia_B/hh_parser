from hh_api_vacancies_parser import VacanciesParser
from hh_bs4_resumes_parser import ResumesParser


def parse_resumes(search_list):
    """
    Возвращает рузультат парсинга резюме.

    :param search_list: Список, в котором первый элемент - строка поиска, остальные синонимы.
    :type search_list: List[str]
    :return: Список словарей с результатами парсинга.
    :rtype: List[Dict[str, Any]]
    """
    resumes_parser = ResumesParser()
    resumes = resumes_parser.get_parsed_data(search_list)
    return resumes


def parse_vacancies(search_list):
    """
    Возвращает рузультат парсинга вакансий.

    :param search_list: Список, в котором первый элемент - строка поиска, остальные синонимы.
    :type search_list: List[str]
    :return: Список словарей с результатами парсинга.
    :rtype: List[Dict[str, Any]]
    """
    vacancies_parser = VacanciesParser()
    vacancies = vacancies_parser.get_parsed_data(search_list)
    return vacancies