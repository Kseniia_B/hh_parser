from functools import reduce  # для распаковки словарей

from base_parser import BaseParser
from helpers import get_request_json, standard_handler, unpack


class VacanciesParser(BaseParser):
    def __init__(self):
        super().__init__()
        self._EMPLOYER_FIELDS = ['name', ]

        #  Конфигурация поиска
        self._MAIN_URL_ARGS = {
            'text': '',
            'area': 1261,  # 1261 - код Свердловской области
            'period': 30,  # за последние 30 дней
            'per_page': 20,
            'specialization': 1,
        }

        #  Основа url поиска
        self._MAIN_URL = 'https://api.hh.ru/vacancies'

        #  Конфигурация поиска полей, которые нас интересуют
        self._FIELDS = [
            {'title': 'Название', 'keys': ['name'], 'handler': standard_handler},
            {'title': 'Зарплата', 'keys': ['salary'], 'handler': self.salary_handler},
            {'title': 'Работодатель', 'keys': ['employer'], 'handler': self.employer_handler},
            {'title': 'url работодателя', 'keys': ['employer', 'url'], 'handler': standard_handler},
            {'title': 'Место проживания', 'keys': ['area'], 'handler': self.area_handler},
            {'title': 'url вакансии', 'keys': ['url'], 'handler': standard_handler},
        ]

    def salary_handler(self, salary):
        """
        Обработчик информации о заработной плате.

        :param salary: Словарь, содержащий информацию о зарплате.
        :type salary: dict
        :return: Возвращает информацию о диапазоне зарплаты и валюту.
        :rtype: str
        """
        if not salary:
            return "Не указано"
        bottom_salary = salary.get('from')  # Нижний предел зп
        top_salary = salary.get('to')  # Верхняя граница зп
        currency = salary.get('currency')  # Валюта зп
        if not top_salary and not bottom_salary:
            return "Не указано"
        if not currency == 'RUR':
            bottom_salary = self._converter.convert_to_rur(bottom_salary, currency) if bottom_salary  else None
            top_salary = self._converter.convert_to_rur(top_salary, currency) if top_salary  else None
            currency = 'RUR'
        if not top_salary or not bottom_salary:
            return f'{bottom_salary or top_salary} {currency}'
        return f'{bottom_salary}-{top_salary} {currency}'

    def employer_handler(self, employer):
        """
        Обработчик информации о работодателе.
    ​
        :param employer: Словарь, содержащий информацию о работодателе.
        :type employer: dict
        :return: Возвращает информацию о работодателе.
        :rtype: str
        """
        if not employer:
            return "Не указано"
        words = []
        for field in self._EMPLOYER_FIELDS:
            value = employer.get(field)
            if value:
                words.append(value)
        return ' '.join(words)

    @staticmethod
    def area_handler(area):
        """
        Обработчик информации о месте проживания.
    ​
        :param area: Словарь, содержащий информацию о месте проживания.
        :type area: dict
        :return: Возвращает информацию о месте проживания.
        :rtype: str
        """
        value = area.get('name')
        if not area or not value:
            return "Не указано"
        return value

    @staticmethod
    def get_pages_count(url, args):
        """
        Получает количество страниц с записями о вакансии.
    ​
        :return: Количество страниц.
        :rtype: int
        """
        response = get_request_json(url, {**args, 'page': 0})
        return response['pages']

    @staticmethod
    def deep_get(dictionary, keys):
        """
        Возвращает значение из словаря по указанным в правильном порядке ключам.

        :param dictionary: Словарь.
        :type dictionary: Dict
        :param keys: Ключи, в порядке которых находится искомое значение в словаре.
        :type keys: List
        """
        return reduce(lambda d, key: d.get(key) if d else None, keys, dictionary)

    def parser(self, page_number):
        """
         Достает нужные поля из ответа get запроса.
    ​
        :param page_number: Номер страницы, с которой собирается информация.
        :type page_number: int
        :return: Список словарей, содержащий информацию о каждой вакансии на странице.
        :rtype: list
        """
        response = get_request_json(self._MAIN_URL, {**self._MAIN_URL_ARGS, 'page': page_number})
        items = response['items']
        result_list = []
        for item in items:
            item_info = {}
            for params in self._FIELDS:
                title = params["title"]
                keys = params["keys"]
                handler = params["handler"]
                value = self.deep_get(item, keys)
                item_info[title] = handler(value)
            result_list.append(item_info)
        return result_list

    def get_parsed_data(self, search_strings, only_matching=True):
        """
        Возвращает список искомых данных.

        :param search_strings: Строки поиска.
        :type search_strings: List[str]
        :param only_matching: Пропускать ли несовпадаюшие
        :type only_matching: bool
        :return: Лист словарей с найдеными данными.
        :rtype: List[Dict[str, Union[str, int]]]
        """
        self._MAIN_URL_ARGS['text'] = search_strings[0]  # устанавливаем строку поиска

        pages_amount = self.get_pages_count(self._MAIN_URL, self._MAIN_URL_ARGS)  # определение количества страниц

        pages_data = []
        for page in range(pages_amount):
            pages_data.append(self.parser(page))  # парсинг страниц

        result_list = unpack(pages_data)  # распаковка результата
        if only_matching:
            # удаление данных с несовпадающими названиями
            result_list = self.get_without_unmatched(result_list, search_strings)
        return result_list
